package com.hg.factorypattern;

public class Main {

    public static void main(String[] args) {
        Enemy ship = EnemyFactory.makeEnemy(Globals.ENEMY_SHIP);
        ship.getStats();

        Enemy tank = EnemyFactory.makeEnemy(Globals.ENEMY_TANK);
        tank.getStats();

        Enemy bomber = EnemyFactory.makeEnemy(Globals.ENEMY_BOMBER);
        bomber.getStats();
    }
}
