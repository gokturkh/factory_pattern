package com.hg.factorypattern;

public class EnemyBomber extends Enemy {
    public EnemyBomber(Globals type, int power, int damage) {
        super(type, power, damage);
    }

    @Override
    protected void attack() {
        System.out.println("Bomber attacked.");
    }

    @Override
    protected void getStats() {
        System.out.println("Enemy type is: " + this.getType());
        System.out.println("Enemy power is: " + this.getPower());
        System.out.println("Enemy damage is: " + this.getDamage());
    }
}
