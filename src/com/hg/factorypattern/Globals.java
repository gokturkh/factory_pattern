package com.hg.factorypattern;

enum Globals {
    ENEMY_TANK,
    ENEMY_SHIP,
    ENEMY_BOMBER
}
