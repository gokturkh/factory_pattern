package com.hg.factorypattern;

abstract public class Enemy {
    private int power;
    private int damage;
    private final Globals type;

    public Enemy(Globals type, int power, int damage) {
        this.type = type;
        this.power = power;
        this.damage = damage;
    }

    public Globals getType() {
        return type;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    protected abstract void attack();

    protected abstract void getStats();
}
