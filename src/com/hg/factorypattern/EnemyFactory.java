package com.hg.factorypattern;

public final class EnemyFactory {
    private static Enemy enemy = null;

    public static Enemy makeEnemy(Globals enemyType) {
        switch (enemyType) {
            case ENEMY_TANK:
                EnemyFactory.enemy = new EnemyTank(Globals.ENEMY_TANK, 70, 65);
                break;
            case ENEMY_SHIP:
                EnemyFactory.enemy = new EnemyShip(Globals.ENEMY_SHIP, 92, 96);
                break;
            case ENEMY_BOMBER:
                EnemyFactory.enemy = new EnemyBomber(Globals.ENEMY_BOMBER, 95, 45);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + enemyType);
        }

        return EnemyFactory.enemy;
    }
}
